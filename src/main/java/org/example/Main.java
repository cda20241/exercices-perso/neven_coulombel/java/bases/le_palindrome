package org.example;

import java.text.Normalizer;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static String stripAccents(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("[^a-zA-Z0-9]", "");
        return str;
    }

    public static String sort(String motPhrase) {
        String sort = "La phrase";
        if (motPhrase.equals(motPhrase.replaceAll("\\s", ""))) {
            // Sans espace notre chaîne de caractères est considérée comme un mot.
            sort = "Le mot";
        }
        return sort;
    }

    public static boolean palindrome(String motPhrase) {
        String motPhraseSimple = stripAccents(motPhrase.toLowerCase());
        for (int i = 0; i < (motPhrase.length() / 2); i++) {
            if (motPhraseSimple.charAt(i) != motPhraseSimple.charAt(motPhraseSimple.length()-(i+1))) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        List<String> exemple = Arrays.asList("Eh ! ça va la vache ?", "Salut");

        for (String str : exemple) {
            if (palindrome(str)) {
                System.out.println(sort(str) + " '" + str + "' est un palindrome !");
            } else {
                System.out.println(sort(str) + " '" + str + "' n'est pas un palindrome...");
            }
        }
    }
}